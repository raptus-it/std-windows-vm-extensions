#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

#Set correct Timezone
Try {
    Set-TimeZone -Id "W. Europe Standard Time"
    Write-Log -Message "Timezone set" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Install basic features
$winfeatures = "SNMP-Service","Telnet-Client"

ForEach ($winfeature in $winfeatures) {
    Try {
        Install-WindowsFeature -IncludeAllSubFeature -IncludeManagementTools -Name $winfeature -ErrorAction Stop
        Write-Log -Message "Feature $winfeature installed" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}

#Basic feature config
Try {
    New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\services\SNMP\Parameters\ValidCommunities" -Name "public" -Value 4 -ErrorAction Stop
    Remove-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\services\SNMP\Parameters\PermittedManagers" -Name "1" -ErrorAction Stop
    Restart-Service -Name "SNMP"
    Write-Log -Message "SNMP configured" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Disable IE Enhanced Security
Try {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0 -Force -ErrorAction Stop
    #Stop-Process -Name Explorer -Force
    Write-Log -Message "IE security for administrator set" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Disable UAC
Try {
    Set-ItemProperty -Path REGISTRY::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name ConsentPromptBehaviorAdmin -Value 0 -ErrorAction Stop
    Write-Log -Message "UAC disabled" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Set Powerstates
$cpws = Get-CimInstance -classname Win32_PowerPlan -Namespace "root\cimv2\power" | Where-Object {$_.IsActive -eq $True}
If ($cpws.ElementName -notmatch "High performance") {
    Try {
        $pws = Get-CimInstance -classname Win32_PowerPlan -Namespace "root\cimv2\power" | Where-Object {$_.ElementName -eq "High performance"}
        Invoke-CimMethod -InputObject $pws -MethodName Activate -ErrorAction Stop
        Write-Log -Message "Powerstate set to High performance" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}

#Register Powershell Repository
Try {
    Install-PackageProvider -Name NuGet -Force -ErrorAction Stop
    Register-PSRepository -Default -ErrorAction Stop
    Write-Log -Message "PS Repository registered" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}
#Enable RDP Access
Try {
    $AdminKey = "HKLM:\System\CurrentControlSet\Control\Terminal Server"
    Set-ItemProperty -Path $AdminKey -Name "fDenyTSConnections" -Value 0 -Force -ErrorAction Stop
    #Stop-Process -Name Explorer -Force
    Write-Log -Message "RDP access enabled" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Set NTPS
Try {
    Start-Service w32time
    $ntps = w32tm /query /computer:$server /configuration | ?{$_ -match 'ntpserver:'} | %{($_ -split ":\s\b")[1]}

    If ($ntps -NotLike "*ch.pool.ntp.org*") {
        Stop-Service w32time
        w32tm /config /manualpeerlist:ch.pool.ntp.org /syncfromflags:ALL
        Start-Service w32time
    }
    w32tm /resync
    Write-Log -Message "NTP set" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Set a bunch of security policies
Try {
    Set-ItemProperty -Path "HKLM:System\CurrentControlSet\Control\Lsa\" -Name "UseMachineId" -Type DWord -Value 1 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:System\CurrentControlSet\Control\Lsa\" -Name "NoLMHash" -Type DWord -Value 1 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:System\CurrentControlSet\Control\Lsa\" -Name "LmCompatibilityLevel" -Type DWord -Value 5 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Lsa\MSV1_0" -Name "NTLMMinClientSec" -Type DWord -Value 537395200 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Lsa\MSV1_0" -Name "NtlmMinServerSec" -Type DWord -Value 537395200 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Lsa\MSV1_0" -Name "allownullsessionfallback" -Type DWord -Value 0 -ErrorAction Stop 
    Set-ItemProperty -Path "HKLM:System\CurrentControlSet\Control\Lsa\pku2u" -Name "AllowOnlineID" -Type DWord -Value 0 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:System\CurrentControlSet\Services\LDAP" -Name "ldapclientintegrity" -Type DWord -Value 1 -ErrorAction Stop
    Set-ItemProperty -Path "HKLM:Software\Microsoft\Windows\CurrentVersion\Policies\System\Kerberos\Parameters" -Name "SupportedEncryptionTypes" -Type DWord -Value 2147483644 -ErrorAction Stop
    Write-Log -Message "Security Policies in place" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Configure Windows Defender Firewall Policies
Try {
    #Public Profile
    $Key = "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile"
    if (-Not(Get-Item $Key)) { New-Item $Key -Force }
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "EnableFirewall" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "DefaultOutboundAction" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "DefaultInboundAction" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "DisableNotifications" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "AllowLocalPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "AllowLocalIPsecPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" -Name "DisableUnicastResponsesToMulticastBroadcast" -Type DWord -Value 0 -ErrorAction Stop

    #Private Profile
    $Key = "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile"
    if (-Not(Get-Item $Key)) { New-Item $Key -Force }
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "EnableFirewall" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "DefaultOutboundAction" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "DefaultInboundAction" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "DisableNotifications" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "AllowLocalPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "AllowLocalIPsecPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" -Name "DisableUnicastResponsesToMulticastBroadcast" -Type DWord -Value 1 -ErrorAction Stop

    #Domain Profile
    $Key = "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile"
    if (-Not(Get-Item $Key)) { New-Item $Key -Force }
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "EnableFirewall" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "DefaultOutboundAction" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "DefaultInboundAction" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "DisableNotifications" -Type DWord -Value 0 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "AllowLocalPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "AllowLocalIPsecPolicyMerge" -Type DWord -Value 1 -ErrorAction Stop
    New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -Name "DisableUnicastResponsesToMulticastBroadcast" -Type DWord -Value 1 -ErrorAction Stop

    Write-Log -Message "Windows Defender Firewall Security Policies in place" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}