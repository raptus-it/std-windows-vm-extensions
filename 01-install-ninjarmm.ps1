#
#
# Bsp: -url "https://eu.ninjarmm.com/agent/installer/d5e25c46-0452-4500-97be-6ac1f1f7b1ef/musidengmbhazurewesteurope-5.3.2695-windows-installer.msi"
#

#PARAM
param(
    [parameter(Mandatory=$true)]
    [string]$url
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

# Download and install NinjaRMM

if (-not (Test-Path $Env:SystemDrive\temp)) {
    New-Item -ItemType Directory $Env:SystemDrive\temp | out-null
}

Try {
    Start-BitsTransfer -Source $url -Destination $Env:SystemDrive\temp\NinjaRMM.msi -ErrorAction Stop
    Start-Process msiexec.exe -Wait -ArgumentList "/i", "$Env:SystemDrive\temp\NinjaRMM.msi", "/qn" -ErrorAction Stop
    Remove-Item -Path $Env:SystemDrive\temp\NinjaRMM.msi -ErrorAction Stop
    Write-Log -Message "NinjaRMM Agent installed"
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error
}