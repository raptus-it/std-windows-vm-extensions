#
#
# Bsp: [-hostname "HOST1"] -domainname "domain.net" -domainUsername "user@domain.net" -domainPassword "pass"
#

#PARAM
param(
    [parameter(Mandatory=$false)]
    [string]$hostname,
    [parameter(Mandatory=$true)]
    [String] $domainname,
    [parameter(Mandatory=$true)]
    [String] $domainUsername,
    [parameter(Mandatory=$true)]
    [String] $domainPassword
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

#Save Creds as secure Object
$passwd = $domainPassword | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList ($domainUsername, $passwd)

#Rename Join Domain
Try {
    If ($hostname) {
        Add-Computer -DomainName $domainname -Force -Credential $cred -NewName $hostname -Restart -ErrorAction Stop
    } else {
        Add-Computer -DomainName $domainname -Force -Credential $cred -Restart -ErrorAction Stop
    }
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error
}